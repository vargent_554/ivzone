import {defineComponent, inject, provide, ref} from "vue";
import {FuncContextKey, LinkViewContextKey, ViewContextKey} from "@/utils/ProvideKeys";
import MixinsEditItem from "@/components/edit/MixinsEditItem";
import {EditContext} from "@/components/view/Context";
import CoreConsts from "@/components/CoreConsts";

export default defineComponent({
  name: 'UModal',
  props: {
    uid: {type: String, required: true, default: CoreConsts.DefaultEditUid}
  },
  mixins: [MixinsEditItem],
  setup(props, {attrs, slots}) {
    let refs = ref(null);
    let visible = ref(false);

    /**
     * @type {LinkContext}
     */
    let linkContext = inject(LinkViewContextKey);
    let editContext = new EditContext(linkContext);
    if(linkContext) {
      editContext.uid = props.uid;
      linkContext.addChildrenContext(editContext)
    }

    return {refs, visible, editContext}
  },
  render() {
    return <AModal {...this.$attrs} v-model={[this.visible, 'visible', ["modifier"]]} v-slots={this.$slots} />
  }
})
